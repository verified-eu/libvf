import BisnodeSweden from './bisnode_se';
import BisnodeNorway from './bisnode_no';
import BisnodeFinland from './bisnode_fi';
import BisnodeDenmark from './bisnode_dk';

export default {

    /** @namespace bisnode.sweden */
    sweden: BisnodeSweden,

    /** @namespace bisnode.norway */
    norway: BisnodeNorway,

    /** @namespace bisnode.finland */
    finland: BisnodeFinland,

    /** @namespace bisnode.denmark */
    denmark: BisnodeDenmark
    
}