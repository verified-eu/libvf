import remote from '../helpers/remote';
import auth from '../auth';

export default class BisnodeSweden {

  /**
   * Get information about a Swedish company
   * 
   * @memberof bisnode.sweden
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company info
   */
  static async getCompanyBaseInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-info?country=se&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

  /**
   * Get credit information about a Swedish company
   * 
   * @memberof bisnode.sweden
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company credit info
   */
  static async getCompanyCreditInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-credit-info?country=se&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

  /**
   * Get information about a Swedish company
   * 
   * @memberof bisnode.sweden
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company info
   */
  static async getCompanyInfo(orgNr, rawRes) {

    let res = await remote.call({
        path: `/bisnode/company?regNumber=${orgNr.replace(/\D/g,'')}${rawRes?'&rawRes=1':''}`,
        method: "GET"
    });
  
    return res.data;

  }

   /**
   * Get information about a Swedish person
   * 
   * @memberof bisnode.sweden
   * 
   * @param {string} ssn SSN to look up 
   * 
   * @returns {json} Person info
   */
  static async getPersonInfo(ssn) {

    let res = await remote.call({
        path: `/bisnode/person?personNumber=${ssn.replace(/\D/g,'')}`,
        method: "GET"
    });
  
    return res.data;

  }


  /**
   * Get credit info about a company
   * 
   * @memberof bisnode.sweden
   * 
   * @param {string} orgNr Organization number to perform a credit check on
   * 
   * @returns {json} Credit info
   */
  static async getCreditInfo(orgNr) {

    let res = await remote.call({
        path: `/bisnode/credit-se?regNumber=${orgNr.replace(/\D/g,'')}`,
        method: "GET"
    });
  
    return res.data;
  }


}