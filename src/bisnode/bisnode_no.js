import remote from '../helpers/remote';

export default class BisnodeNorway {


  /**
   * Get information about a Norweigan company
   * 
   * @memberof bisnode.norway
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company info
   */
  static async getCompanyBaseInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-info?country=no&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

  /**
   * Get credit information about a Norwegian company
   * 
   * @memberof bisnode.norway
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company credit info
   */
  static async getCompanyCreditInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-credit-info?country=no&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

  /**
   * Get credit info about a company
   * 
   * @memberof bisnode.norway
   * 
   * @param {string} orgNr Organization number to perform a credit check on
   * 
   * @returns {json} Credit info
   */
  static async getCreditInfo(orgNr) {

    let res = await remote.call({
        path: `/bisnode/credit-no?regNumber=${orgNr.replace(/\D/g,'')}`,
        method: "GET"
    });
  
    return res.data;

  }


}