import remote from '../helpers/remote';

export default class BisnodeDenmark {

  /**
   * Get information about a Danish company
   * 
   * @memberof bisnode.denmark
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company info
   */
  static async getCompanyBaseInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-info?country=dk&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

  /**
   * Get credit information about a Danish company
   * 
   * @memberof bisnode.denmark
   * 
   * @param {string} orgNr Organization number to look up 
   * 
   * @returns {json} Company credit info
   */
  static async getCompanyCreditInfo(orgNr, language) {

    let res = await remote.call({
        path: `/bisnode/company-credit-info?country=dk&regNumber=${orgNr.replace(/\D/g,'')}${language?'&language='+language:''}`,
        method: "GET"
    });
  
    return res.data;

  }

}