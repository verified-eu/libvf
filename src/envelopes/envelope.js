import remote from '../helpers/remote';
import Document from '../documents/document';

/**
 * The envelope instance returned by {@link envelopes.create}
 * @class Envelope
 * @property {string} id The envelope id 
 * @property {object} data The raw envelope data from the last reflection
 * @property {Document[]} documents The envelopes documents
 */
export default class Envelope {

  constructor(id) {
    this.id = id;
    this.data = {};
    this.documents = [];
  }

  /**
   * Fetches the envelope data from API and parses the data, instanciating documents etc
   * @instance
   * @memberof Envelope
   */
  async reflect() {

    let res = await remote.call({
      path: `/envelopes/${this.id}`,
      method: "GET"
    });

    this.data = res.data;

    this.deserialize();

  }

  /**
   * Makes a raw PUT request to the API on the envelope
   * @instance
   * @memberof Envelope
   * @param {object} data The payload to be sent
   */
  async put(data) {

    let res = await remote.call({
      path: `/envelopes/${this.id}`,
      method: "PUT",
      body: data
    });

    return res;

  }

  /**
   * Adds a new recipient to the envelope
   * @instance
   * @memberof Envelope
   * @param {object} recipient The recipient data
   * @param {string} recipient.givenName The recipients first name
   * @param {string} recipient.familyName The recipients last name
   * @param {string} recipient.language The language code. "en_EN", "nb_NO", "sv_SE" etc
   * @param {string} recipient.signingMethod The recipients signing method. "email", "touch-sign", "bankid-no", "bankid-se"
   * @param {string} recipient.email The recipients email address
   * @param {number} recipient.order The recipients order in the envelope process
   * @param {object} recipient.role The role of the recipient
   * @param {string} recipient.role.action The recipients action. "sign", "review", "edit"
   * @param {string} recipient.role.label The recipients label that will appear in the Verified Webapp
   * @param {string} recipient.role.name The role name as described in the envelope descriptor
   */
  async addRecipient(recipient) {

    let res = await remote.call({
      path: `/envelopes/${this.id}/recipients`,
      method: "POST",
      body: recipient
    });

  }

  /**
   * Publish the envelope
   * @instance
   * @memberof Envelope
   */
  async publish() {

    let res = await remote.call({
      path: `/envelopes/${this.id}/publish-status`,
      method: "PUT",
      body: { published: true }
    });

  }

   /**
   * Poll for a sign token
   * @instance
   * @memberof Envelope
   * @param {string} flowId The flow id of the envelope as specified in the descriptor
   * @returns {string} JWT with permissions to sign the documents
   */
  async getSignToken(flowId) {

    return new Promise((resolve, reject) => {

      let self = this;
      const attempts = 10;

      let pollForSignToken = setInterval(() => {

        remote.call({
          path: `${flowId}/jobs/${self.id}`,
          method: "GET"
        })
        .then((res) => {
          if(res.data.signToken && res.data.signToken != "NA") {
            clearInterval(pollForSignToken);
            resolve(res.data.signToken);
          }
        })
        .catch((err) => {
          clearInterval(pollForSignToken);
          console.log(err);
        });

      }, 1000);

    });

  }

  /**
   * Get sign link for a recipient
   * @instance
   * @memberof Envelope
   * @param {string} id The id of the recipient
   * @param {string} redirectTo The redirectTo url to goto once signed
   * @returns {string} Full URL to signpage
   */
  async getSignLink(id, redirectTo) {
    let res = await remote.call({
      path: `/envelopes/${this.id}/jobs/get.sign.link`,
      method: "POST",
      body: {
        recipient: {
          id
        },
        redirectTo
      }
    })
    if (!res.data.getSignLink) throw new Error('no signlink found')
    const recipient = res.data.getSignLink.recipient[id]
    if (recipient.url) return recipient.url
    else throw new Error(recipient.message)
  }

    /**
   * Poll for a sign url
   * @instance
   * @memberof Envelope
   * @param {object} args Args object with optional .recipientId and .redirectTo
   * @returns {string} Full URL to signpage
   */
     async getSignUrl(args) {
      return new Promise((resolve, reject) => {
        let self = this
        let attempts = 60
        const pollForSignUrl = async () => {
          await self.reflect()
  
          let hasDocumentUploaded = self.documents && self.documents[0] && self.documents[0].data && self.documents[0].data.files ? true : false
          if (hasDocumentUploaded) {
            let recipientId = null
            let redirectTo = null
            if(typeof args == 'object' && args !== null) {
              recipientId = args.recipientId
              redirectTo = args.redirectTo
            }
  
            if (!recipientId) {
              const recipients = self.data && self.data.recipients && self.data.recipients ? self.data.recipients.slice(): null
              if (recipients && recipients.length) {
                recipients.sort((a, b) => { return a.order - b.order })
                for (let i = 0; i < recipients.length; i++) {
                  const r = recipients[i]
                  if (r && r.role && r.role.action == 'sign') {
                    recipientId = r.id
                    break
                  }
                }
              }
            }
  
            if (recipientId) {
              await self.getSignLink(recipientId, redirectTo)
              .then(link => { resolve(link) })
              .catch(err => { reject(err) })
            } else {
              throw new Error('Recipient not found')
            }
          } else {
            throw new Error('Document is not yet uploaded')
          }
        }
        const doPoll = () => {
          pollForSignUrl()
          .then((link) => {
            resolve(link)
          })
          .catch((err) => {
            if(attempts--) {
              setTimeout(doPoll, 1000)
            } else {
              reject(new Error('Max pollForSignUrl attempts exceeded!'))
            }
          })
        }
        doPoll()
      })
    }
  

  /**
   * Poll for a sign token
   * @instance
   * @memberof Envelope
   * @param {object} args Args object with optional flowId
   * @returns {string} Full URL to signpage
   */
  async getDirectSignUrl(args) {
    return new Promise((resolve, reject) => {
      let self = this
      let attempts = 60
      const pollForSignUrl = async () => {
        await self.reflect()

        const hasDocumentUploaded = self.documents && self.documents[0] && self.documents[0].data && self.documents[0].data.files ? true : false
        if (hasDocumentUploaded) {
          const flowId = args && args.flowId || self.data.flow && self.data.flow.id || self.data.descriptor.flow && self.data.descriptor.flow && self.data.descriptor.flow.id
          const flowJob = await remote.call({
            path: `${flowId}/jobs/${self.id}`,
            method: "GET"
          })
          if(flowJob.data.signToken && flowJob.data.signToken != "NA" && flowJob.data.signLink) {
            resolve(flowJob.data.signLink)
          } else {
            throw new Error('signLink not found')
          }
        } else {
          throw new Error('Document is not yet uploaded')
        }
      }
      const doPoll = () => {
        pollForSignUrl()
        .then((link) => {
          resolve(link)
        })
        .catch(() => {
          if(attempts--) {
            setTimeout(doPoll, 1000)
          } else {
            reject(new Error('Max pollForSignUrl attempts exceeded!'))
          }
        })
      }
      doPoll()
    })
  }

  deserialize() {
    this.documents = [];
    for(let document of this.data.documents) {
      this.documents.push(new Document(document.id, this.id, document));
    }
  }

  /**
   * Get the first document in the envelope
   * @instance
   * @memberof Envelope
   * @returns {Document} The first document in the envelope
   */
  firstDocument() {
    if(this.documents.length > 0) {
      return this.documents[0];
    }

    return null;
  }

  /**
   * Get the first template in the first document of the envelope
   * @instance
   * @memberof Envelope
   * @returns {Template} The first template in the first document
   */
  firstTemplate() {
    if(this.documents.length > 0 && this.documents[0].template) {
      return this.documents[0].template;
    }

    return null;
  }

  /**
   * Creates a new document
   * @instance
   * @memberof Envelope
   * @param {object} data The payload to be sent with the request to the API
   * 
   * @return {object} Raw HTTP response form the API
   */
  async createDocument(data) {
    let res = await remote.call({
      path: `/envelopes/${this.id}/documents`,
      method: "POST",
      body: data
    });

    return res;
  }

   /**
   * Poll for bearer token
   * @instance
   * @memberof Envelope
   * @param {string} flowId The flow id of the envelope as specified in the descriptor
   * @returns {string} JWT with permissions to make actions on the envelope
   */
  async getBearerToken(flowId) {

    return new Promise((resolve, reject) => {

      let self = this;
      const attempts = 10;

      let pollForBearerToken = setInterval(() => {

        remote.call({
          path: `${flowId}/jobs/${self.id}`,
          method: "GET"
        })
        .then((res) => {
          if(res.data.token && res.data.token != "NA") {
            clearInterval(pollForBearerToken);
            resolve(res.data.token);
          }
        })
        .catch((err) => {
          clearInterval(pollForBearerToken);
          console.log(err);
        });

      }, 1000);

    });

  }

  /**
   * Run flow job
   * @instance
   * @memberof Envelope
   * @param {string} taskName The job name specified in the flow
   * @param {body} body The request body sent to the flow job
   * @returns {string} Raw response data from the flow job
   */
  async runFlowTask(taskName, body) {

    let res = await remote.call({
      path: `/envelopes/${this.id}/jobs/${taskName}`,
      method: "POST",
      body
    });

    return res.data;

  }

}
