import remote from '../helpers/remote';
import qs from 'query-string';
import jwt from 'jwt-decode';

export default class OIDCNorway {
  
  //todo: Document new methods
  static async getAuthUrlAndStoreData(redirectUrl, data) {
  
    redirectUrl = redirectUrl || window.location.pathname;
    data = data || {};
    
    let res = await remote.call({
      path: `/bankid-oidc/auth`,
      method: "POST",
      body: {
        redirectUrl: redirectUrl,
        data: data
      }
    });
    
    return res.data;
    
  }
  
  static urlHasSessionId() {
    
    let index = window.location.href.indexOf('#');
    let url = window.location.href.substr(0, index) + '?' + window.location.href.substr(index + 1);
    let parsed = !!location.search ? qs.parse(location.search) : qs.parse(url);
    
    return !!parsed.state;
    
  }
  
  static async getStoredData(sessionId) {
    
    let index = window.location.href.indexOf('#');
    let url = window.location.href.substr(0, index) + '?' + window.location.href.substr(index + 1);
    let parsed = !!location.search ? qs.parse(location.search) : qs.parse(url);
    
    let res = await remote.call({
      path: `/bankid-oidc/auth/session`,
      method: "GET",
      params: {
        sessionId: sessionId || parsed.state
      }
    });
    
    return res.data;
    
  }
  
  /**
   * Returns a URL in which to redirect the user to
   * 
   * @memberof oidc.norway
   * 
   * @param {string} redirect 
   * 
   * @returns {json} Json object with a url key, with the actual url as value
   */
  static async getAuthUrl(redirect) {

    let redirectPath = redirect || window.location.pathname;

    let res = await remote.call({
        path: `/bankid-oidc/authentication?redirect_path=${redirectPath}`,
        method: "GET"
    });
  
    return res.data.url;

  }

  /**
   * Checks if the browser URL contains id_token and access_token parameters
   * 
   * @memberof oidc.norway
   *
   * @returns {boolean}
   */
  static urlHasNecessaryTokens() {

    let index = window.location.href.indexOf('#');
    let url = window.location.href.substr(0, index) + '?' + window.location.href.substr(index + 1);
    let parsed = !!location.search ? qs.parse(location.search) : qs.parse(url);

    return !!(parsed.access_token && parsed.id_token);
    
  }
  
  /**
   * Get AML data
   *
   * @memberof oidc.norway
   *
   * @param {object} [opts={}] Options
   * @param {string} [opts.id_token=id_token from url] BankID id_token
   * @param {string} [opts.access_token=access_token from url] BankID access_token
   *
   * @returns {json} Raw AML API response from BankID
   *
   */
  static async getAmlData(opts) {
    
    if(!opts) opts = {};
    
    let index = window.location.href.indexOf('#');
    let url = window.location.href.substr(0, index) + '?' + window.location.href.substr(index + 1);
    let parsed = !!location.search ? qs.parse(location.search) : qs.parse(url);
    
    let res = await remote.call({
      path: `/bankid-oidc/aml`,
      method: "GET",
      headers: {
        id_token: opts.id_token || parsed.id_token,
        access_token: opts.access_token || parsed.access_token,
        // sessionId: opts.sessionId || parsed.state
      }
    });
    
    return res.data;
    
  }

  /**
   * Get company info
   *
   * @memberof oidc.norway
   *
   * @param {string} orgNr Organization number
   * @param {Array.<string>} [expands=[]] Expands https://aml.bankidapis.no/#operation/Organization_RetrieveOrganizationInformation
   * @param {string} [nationality=NO] Nationality
   *
   * @returns {json} Information about the company from BankID
   *
   */
  static async getCompanyInfo(orgNr, expands, nationality) {

    if(!expands)
      expands = [];

    if(!nationality)
      nationality = "NO";

    let res = await remote.call({
        path: `/bankid-oidc/organization/noauth?organizationNumber=${orgNr.toString().replace(/\D/g,'')}&nationality=${nationality}&expands=${expands.toString()}`,
        method: "GET"
    });

    return res.data;

  }

    /**
   * Get Company Report
   *
   * @memberof oidc.norway
   *
   * @param {string} pdfUrl Report URL
   *
   * @returns {binary} A PDF file with content of the report created by BankID
   *
   */
  static async getCompanyReport(pdfUrl) {
  
  return new Promise((resolve, reject) => {
      let attempts = 1
  
      let pollForPdf = setInterval(() => {
        remote.call({
          path: `/bankid-oidc/organization/pdf?pdfUrl=${ pdfUrl }`,
          method: "GET"
        })
          .then((res) => {
            console.log(`Poll for PDF ${ attempts++ }:`, res)
        
            if(res.data) {
              clearInterval(pollForPdf)
              resolve(res.data)
            }
          })
          .catch((err) => {
            clearInterval(pollForPdf)
            console.log(err)
          });
      }, 1000)
    })
  }
  
  /**
   * Get person info
   *
   * @memberof oidc.norway
   *
   * @param {object} params Parameters to be included. Everything in this object gets used as parameters in the search
   * @param {string} params.dateOfBirth Date of birth, format YYYYMMDD. National ID number or date of birth is required
   * @param {string} params.firstName First name
   * @param {string} params.lastName Last name
   * @param {string} params.ssn National ID number. National ID number or date of birth is required
   * @param {string} [params.countryOfSsn=NO] Origin country for the SSN (two letters, ISO 3166). When using SSN this is required
   * @param {string} [params.language=NO] Language of the report (two letters ISO 639-1)
   * @param {array} params.expands Possibilities: "keyInformation.birthplace" "keyInformation.citizenship" "address.postal" "address.national" "address.historic" "address.numberOfResidents" "aml" "links.reports.aml"
   * @param {string} [params.matchMode=exact] Possibilities: "exact" or "fuzzy". Match mode on name. Fuzzy mode gives matches on names that are not completely similar
   * @param {boolean} [params.addPdfAppendix=false] Add extra attachment with descriptions to report
   *
   * @returns {json} Information about the person from BankID
   *
   */
  static async getPersonInfo(params) {
    
    if(!params.countryOfSsn)
      params.countryOfSsn = 'NO'
  
    if(!params.language)
      params.language = 'NO'
  
    if(!params.expands)
      params.expands = ['aml']
    
    if(!params.matchMode)
      params.matchMode = 'exact'
  
    if(!params.addPdfAppendix)
      params.addPdfAppendix = false
    
    let paramString = ''
    Object.keys(params).forEach((p) => {
      paramString += `&${p}=${params[p].toString()}`
    })
    
    let res = await remote.call({
      path: `/bankid-oidc/person/noauth?${paramString}`,
      method: 'GET'
    });
    
    return res.data
    
  }
  
  /**
   * Get person report
   *
   * @memberof oidc.norway
   *
   * @param {string} pdfUrl Report URL
   *
   * @returns {binary} A PDF file with content of the report created by BankID
   *
   */
  static async getPersonReport(pdfUrl) {
    
    return new Promise((resolve, reject) => {
      let attempts = 1
  
      let pollForPdf = setInterval(() => {
        remote.call({
          path: `/bankid-oidc/person/pdf?pdfUrl=${ pdfUrl }`,
          method: "GET"
        })
          .then((res) => {
            console.log(`Poll for PDF ${ attempts++ }:`, res)
        
            if(res.data) {
              clearInterval(pollForPdf)
              resolve(res.data)
            }
          })
          .catch((err) => {
            clearInterval(pollForPdf)
            console.log(err)
          });
      }, 1000)
    })
  }
  
  /**
   * Decodes and returns the id_token and access_token parameter from the Url
   * @memberof oidc.norway
   * @returns {json} id_token and access_token object
   */
  static decodeUrlTokens() {

    if(!this.urlHasNecessaryTokens())
      throw Error("id_token and access_token not present in Url");
    
    let index = window.location.href.indexOf('#');
    let url = window.location.href.substr(0, index) + '?' + window.location.href.substr(index + 1);
    let parsed = !!location.search ? qs.parse(location.search) : qs.parse(url);

    return {
      id_token: jwt(parsed.id_token),
      access_token: jwt(parsed.access_token)
    }

  }

}